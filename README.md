# README #
This repository is for the Udacity Intro to Hadoop (in collaboration with Cloudera).

The source files here are my solutions to the various exercises and projects.

If you are attending the course, please do not use this source to 'pass'.

This is written in Python using a java streaming library.

Thank you