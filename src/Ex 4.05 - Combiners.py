#!/usr/bin/python
import os
import sys

def mapper():
#     fin = open('purchases.txt','r')
    dictSum = dict()
    for line in sys.stdin:
        date, time, store, item, cost, payment = line.strip().split('\t')
        weekday = datetime.strptime(date, "%Y-%m-%d").weekday()
        if weekday in dictSum:
            dictSum[weekday][0] += 1
            dictSum[weekday][1] += float(cost)
        else:
            dictSum[weekday] = [1,float(cost)]
    for value in dictSum:
        print '{0}\t{1}\t{2}'.format(value, dictSum[value][0], dictSum[value][1])
        
mapper()
 
import sys
 
def reducer():
    dictDays = {0 : 'Monday',
                1 : 'Tuesday',
                2 : 'Wednesday',
                3 : 'Thursday',
                4 : 'Friday',
                5 : 'Saturday',
                6 : 'Sunday'}
    
    dictValues = {}
    
    for line in sys.stdin:
        dayKey, count, total = line.strip().split('\t')
        if int(dayKey) in dictValues:
            dictValues[int(dayKey)][0] += int(count)
            dictValues[int(dayKey)][1] += float(total)
        else:
            dictValues[int(dayKey)] = [int(count), float(total)]
    
    for value in dictValues:
        print '{0}\t{1}'.format(dictDays[value], (dictValues[values][1]/dictValues[values][0])) 
    
#     oldKey = None
#     if oldKey and oldKey != thisKey:
#         pass
#     oldKey = thisKey
    
reducer() 
