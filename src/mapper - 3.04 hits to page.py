#! /usr/bin/env python

import sys


# fin = open('access_log')
# line = fin.readline()
# while line != '':
for line in sys.stdin:
    data = line.strip().split()
    if len(data) == 10:
        ip, identity, username, datetime, zone, requestCommand, requestLine, protocol, statusCode, objSize = data
        print "{0}\t{1}".format(requestLine, ip)
#     line = fin.readline()