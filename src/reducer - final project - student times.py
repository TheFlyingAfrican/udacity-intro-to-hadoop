#! /usr/bin/python

import sys
import csv

def findMax(hourDict):
    """
    returns tuple of max hours and a list of the hours, given a dictionary
    if only 1 hour is the max, only it is returned
    if more than 1 hour ties for the max, then each of those are returned.
    """
    max = 0
    maxHour = []
    for hour in hourDict:
        if hourDict[hour] > max:
            max = hourDict[hour]
            maxHour = [hour]
        elif hourDict[hour] == max:
            maxHour.append(hour)
    return max, maxHour
            
    
def reducer():
#     reader = csv.reader(open('forum_users.tsv'), delimiter ='\t')
#     reader = csv.reader(open('forum_node_output.tsv'), delimiter='\t')
    reader = csv.reader(sys.stdin, delimiter='\t')
#     writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    
    userHours = dict()
    for line in reader:
        
        try:
            key = int(line[0])
        except exceptions.ValueError:
            continue
        try:
            onHour = int(line[1])
        except exceptions.ValueError:
            continue
        if key in userHours:
            if onHour in userHours[key]:
                userHours[key][onHour] += 1
            else:
                userHours[key][onHour] = 1
        else:
            userHours[key] = dict()
            userHours[key][onHour] = 1
    for user in userHours:
        max, maxHour = findMax(userHours[user])
        maxHour.sort()
        for hour in maxHour:
            print (str(user) + '\t' + str(hour))
            
#         print (str(user) + '\t' + str(maxHour) + '\t' + str(max))
        


reducer()