#! /usr/bin/python

import sys
import csv

def findOrderedList(listData, item, compareItem = 0):
    """
    function:
      Searches sortedArray[first]..sortedArray[last] for key.  
    returns: index of the matching element if it finds key, 
            otherwise  -(index where it could be inserted)-1.
    parameters:
      sortedArray in  array of sorted (ascending) values.
      first, last in  lower and upper subscript bounds
      key         in  value to search for.
    returns:
      index of key, or -insertion_position -1 if key is not 
                    in the array. This value can easily be
                    transformed into the position to insert it.
    """
    found = False
    pivotPoint = 0
    low = 0
    high = len(listData)-1 # to match the index
    while (low <= high):
        pivotPoint = (high + low) // 2
        if item[compareItem] > listData[pivotPoint][compareItem]:
            low = pivotPoint + 1
        elif item[compareItem] < listData[pivotPoint][compareItem]:
            high = pivotPoint - 1
        else:
            return pivotPoint
    return -(low + 1)

def reducer():
#     reader = csv.reader(open('forum_node_output.tsv'), delimiter='\t')
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    topDict = dict()
    
    for line in reader:
        if line[0] in topDict:
            topDict[line[0]] += int(line[1])
        else:
            topDict[line[0]] = int(line[1])
    top10 = []
    for item in topDict.items():
        insertElement = findOrderedList(top10, list(item),1)
        if insertElement < 0:
            top10.insert(-(insertElement+1), list(item))
        else:
            top10.insert(insertElement, list(item))
        if len(top10) > 10:
            top10.pop(0) #remove first element (smallest)
        
    for i in range(9,-1,-1):
        writer.writerow(top10[i])

reducer()