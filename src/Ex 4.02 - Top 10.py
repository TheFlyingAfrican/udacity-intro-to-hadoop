#!/usr/bin/python
import sys
import csv

def findMaxPos(list, n): 
#returns position in list where n is >= list[position]
    for i in range(0,len(list)):
        if n >= list[i][0]:
            foundPos = i
            return foundPos
    return -1 #no bigger element found, return -1
             

def mapper():
    top10 = []
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

    for line in reader:
        if len(line) != 6:
            continue
        else:
            insertElement = findMaxPos(top10, len(line[4]))
            if insertElement != -1:
                if len(top10) == 10:
                    top10.pop() #remove last element (smallest)
                top10.insert(insertElement, [len(line[4]),line]) #insert new bigger element
            else:
                if len(top10) < 10:
                    top10.append([len(line[4]),line]) #keep adding at the back of the list
    #swap list around (smallest at front)
    for i in range(0, len(top10)):
        top10.insert(i, top10.pop())
    for i in range(0, len(top10)):
        writer.writerow(top10[i][1])



test_text = """\"\"\t\"\"\t\"\"\t\"\"\t\"333\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"88888888\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"1\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"11111111111\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"1000000000\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"22\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"4444\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"666666\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"55555\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"999999999\"\t\"\"
\"\"\t\"\"\t\"\"\t\"\"\t\"7777777\"\t\"\"
"""

# This function allows you to test the mapper with the provided test string
def main():
    import StringIO
    sys.stdin = StringIO.StringIO(test_text)
    mapper()
    sys.stdin = sys.__stdin__

main()
