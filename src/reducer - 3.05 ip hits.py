#!/usr/bin/python

import sys
numReq = 0
oldKey = None

for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) != 2:
        continue
    ip, requestLine = data
    if oldKey and oldKey != ip: #not first run and ip has changed
        print oldKey + '\t' + str(numReq)
        numReq = 0 # reset counter
    oldKey = ip
    numReq += 1

if oldKey != None:
    print oldKey + '\t' + str(numReq)

        
