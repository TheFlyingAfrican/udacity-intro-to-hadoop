#!/usr/bin/python

import sys
# inputData = ['10.192.168.1\t/assets/js/the-associates.js',
#              '10.192.168.1\t/assets/js/the-associates.js',
#              '10.192.168.1\t/assets/js/the-associates.js',
#              '10.192.168.3\t/assets/js/the-associates.html',
#              '10.192.168.1\t/assets/js/the-associates.js',
#              '10.192.168.1\t/assets/js/the-associates.js',
#              '10.192.168.5\t/assets/js/the-associates.js',
#              '10.192.168.1\t/assets/js/the-associates.html',
#              '10.192.168.1\t/assets/js/the-associates.js',
#              '10.192.163.2\t/assets/js/the-associates.js',
#              '10.192.162.1\t/assets/js/the-associates.html']
numReq = 0
oldKey = None

# for line in inputData:
for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) != 2:
        continue
    requestLine, ip = data
    if oldKey and oldKey != requestLine: #not first run and requestLine has changed
        print oldKey + '\t' + str(numReq)
        numReq = 0 # reset counter
    oldKey = requestLine
    numReq += 1

if oldKey != None:
    print oldKey + '\t' + str(numReq)

        
