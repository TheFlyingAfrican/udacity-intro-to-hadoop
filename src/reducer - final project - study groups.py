#! /usr/bin/python

import sys
import csv

def reducer():
#     reader = csv.reader(open('forum_node_output.tsv'), delimiter='\t')
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    
    threadDict = dict()
    
    for line in reader: # data comes in as abs_parent_id, post_id, author_id, node_type, parent_id
        if len(line) == 5:
            abs_parent_id, post_id, author_id, node_type, parent_id = line
        if (abs_parent_id == '\\N'):
            abs_parent_id = post_id
        else:
            try:
                int(abs_parent_id)
            except:
                continue
        if abs_parent_id in threadDict:
            threadDict[abs_parent_id].append(int(author_id))
        else:
            threadDict[abs_parent_id] = [int(author_id)]
    for item in threadDict.items():
        item[1].sort()
        writer.writerow(item)
        
reducer()