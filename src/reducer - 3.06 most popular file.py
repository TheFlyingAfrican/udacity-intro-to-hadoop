# #!/usr/bin/python
# 
# import sys
# numReq = 0
# topHits = 0
# topKey = None
# oldKey = None
# 
# for line in sys.stdin:
#     data = line.strip().split("\t")
#     
#     if len(data) != 3:
#         continue #something wrong with the line, so ignore it
#     
#     requestLine, ip, asset = data
#     
#     if oldKey and oldKey != requestLine: #not first run and requestLine has changed
#         if numReq > topHits:
#             topHits = numReq
#             topKey = oldKey
#         numReq = 0 # reset counter
#         
#     oldKey = requestLine
#     numReq += 1
# 
# print topKey + '\t' + str(topHits)



#!/usr/bin/python

import sys
numReq = 0
topHits = 0

topKey = None
oldKey = None

assets = {}

# oldAsset = None
# assetHits = 0
# topAsset = None
# topAssetHits = 0

# string = ['/assets/css/associate.css\t10.10.10.11\tassociate.css',
#           '/assets/css/associate.css\t10.10.10.11\tassociate.css',
#           '/assets/css/associate.css\t10.10.10.11\tassociate.css',
#           '/assets/css/json.css\t10.11.112.13\tjson.css',
#           '/assets/css/json.css\t10.11.112.13\tjson.css',
#           '/assets/css/json.css\t10.11.112.13\tjson.css',
#           '/assets/css/json.css\t10.11.112.13\tjson.css',
#           'http://www.www.com/assets/css/associate.css\t10.10.10.11\tassociate.css',
#           'http://www.www.com/assets/css/associate.css\t10.10.10.11\tassociate.css',
#           'http://www.www.com/assets/css/associate.css\t10.10.10.11\tassociate.css']
# 
# for line in string:
for line in sys.stdin:
    data = line.strip().split("\t")
    
    if len(data) != 3:
        continue #something wrong with the line, so ignore it
    
    requestLine, ip, asset = data
    
    if asset in assets:
        assets[asset] += 1
    else:
        assets[asset] = 1
    
    if oldKey and oldKey != requestLine: #not first run and requestLine has changed
        if numReq > topHits:
            topHits = numReq
            topKey = oldKey
        numReq = 0 # reset counter
        
#         if assetHits > topAssetHits: #oldAsset is greatest hit
#             if oldAsset == topAsset:
#                 topAssetHits += assetHits #add it on
#             else:
#                 topAssetHits = assetHits
#                 topAsset = oldAsset
#         if oldAsset != asset: #different asset and requestLine has changed
#             assetHits = 0 #reset the counter
        
    oldKey = requestLine
    numReq += 1
#     oldAsset = asset
#     assetHits += 1
# print topKey + '\t' + str(topHits) + '\t' + topAsset + '\t' + str(topAssetHits)
print topKey + '\t' + str(topHits) + assets[topKey[topKey.rfind('/')+1:]]