#! /usr/bin/python

#structure of forum post:
#id, title, tag_names, author_id, body, node_type, parent_id,  abs_parent_id, added_at, score, state_string, last_edited_id,
#last_activity_by_id, last_activity_at, active_revision_id, extra, extra_ref_id, extra_count, marked

import sys
import csv
            
def mapper():
    tagDict = dict()
    reader = csv.reader(sys.stdin, delimiter='\t')
#     reader = csv.reader(open('forum_node.sample.tsv'), delimiter ='\t')
#     reader = csv.reader(open('forum_node.tsv'), delimiter ='\t')
#     writer = csv.writer(open('forum_node_output.tsv','w'), delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)    
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

    for line in reader:
        if len(line) != 19:
            continue
        else:
            for word in line[2].split(' '):
                if word.lower() in tagDict:
                    tagDict[word.lower()] += 1
                else:
                    tagDict[word.lower()] = 1
    for item in tagDict.items():
        writer.writerow(list(item))

mapper()

