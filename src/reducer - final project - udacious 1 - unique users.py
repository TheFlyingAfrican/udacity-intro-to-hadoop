#! /usr/bin/python

#returns the posts with the most unique users (widest audience)

import sys
import csv

def findOrderedList(listData, item, compareItem = 0):
    """
    function:
      Searches sortedArray[first]..sortedArray[last] for key.  
    returns: index of the matching element if it finds key, 
            otherwise  -(index where it could be inserted)-1.
    parameters:
      sortedArray in  array of sorted (ascending) values.
      first, last in  lower and upper subscript bounds
      key         in  value to search for.
    returns:
      index of key, or -insertion_position -1 if key is not 
                    in the array. This value can easily be
                    transformed into the position to insert it.
    """
    found = False
    pivotPoint = 0
    low = 0
    high = len(listData)-1 # to match the index
    while (low <= high):
        pivotPoint = (high + low) // 2
        if item[compareItem] > listData[pivotPoint][compareItem]:
            low = pivotPoint + 1
        elif item[compareItem] < listData[pivotPoint][compareItem]:
            high = pivotPoint - 1
        else:
            return pivotPoint
    return -(low + 1)

def reducer():
#     reader = csv.reader(open('forum_node_output.tsv'), delimiter='\t')
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    
    threadDict = dict()
    
    for line in reader: # data comes in as abs_parent_id, post_id, author_id, node_type, parent_id
        if len(line) == 5:
            abs_parent_id, post_id, author_id, node_type, parent_id = line
            if (abs_parent_id == '\\N'):
                abs_parent_id = post_id
            else:
                try:
                    int(abs_parent_id)
                except:
                    continue
            if abs_parent_id in threadDict:
                threadDict[abs_parent_id].append(int(author_id))
            else:
                threadDict[abs_parent_id] = [int(author_id)]
    sortedList = list()
    for item in threadDict.items():
#         item[1].sort()
        row = list()
        row.append(item[0])
        row.append(len(set(item[1])))
        insertElement = findOrderedList(sortedList, row, 1)
        if insertElement < 0:
            sortedList.insert(-(insertElement+1), row)
        else:
            sortedList.insert(insertElement, row)  
    for i in range(len(sortedList)-1, -1, -1):
        writer.writerow(sortedList[i])
        
reducer()