#!/usr/bin/python
import os
import sys
import csv

#structure of forum post:
#id, title, tag_names, author_id, body, node_type, parent_id,  abs_parent_id, added_at, score, state_string, last_edited_id,
#last_activity_by_id, last_activity_at, active_revision_id, extra, extra_ref_id, extra_count, marked

def insertOrderedList(listData, item): 
    #inserts item into list in ascending order {list[0] is smallest item}
    #assumes list is already ordered
    #returns list with item inserted at appropriate location
    i = 0
    while (i < len(listData)):
        if listData[i] > item:
            listData.insert(i, item)
            return listData
        else:
            i += 1
    listData.append(item)
    return listData

def splitString(dataString):
    punct = ' .!?:;"()<>[]#$=-/,\''
    wordList = []
    word = ''
    for i in range(0, len(dataString)):
        if dataString[i] in punct:
            if word != '':
                wordList.append(word.strip())
            word = ''
            continue
        else:
            word += dataString[i]
    if word != '':
        wordList.append(word.strip())
    return wordList

def mapper():
    fin = open('forum_node.tsv','r')
    finW = open('forum_node_map.tsv','w')
    reader = csv.reader(fin, delimiter='\t', quotechar='"')
    writer = csv.writer(finW, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
#     reader = csv.reader(sys.stdin, delimiter='\t')
#     writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    keywords = {}
    for line in reader:
        if len(line) != 19:
            continue
        else:
            body = splitString(line[4])
            for i in range(0, len(body)):
                if body[i].lower() in keywords:
                    keywords[body[i].lower()][0] += 1
                    # only insert node if not already in the list
#                     if line[0] not in keywords[body[i].lower()][1]:
#                         insertOrderedList(keywords[body[i].lower()][1], line[0])
                    keywords[body[i].lower()][1].append(line[0]) #insert regardless if it already exists
                else:
                    keywords[body[i].lower()] = [1,[line[0]]]
    for item in keywords:
#         writer.writerow(item + '\t' + str(keywords[item][0]) + '\t' + keywords[item][1])
#         #decompose dictionary into multiple (key,value) pair, for hadoop streaming we can't write (key,List<values>)
        for node in keywords[item][1]:
            writer.writerow((item, node))
# #             print (item, '\t', node)
    finW.close()
    
mapper()

#!/usr/bin/python
import os
import sys
import csv

#structure of forum post:
#id, title, tag_names, author_id, body, node_type, parent_id,  abs_parent_id, added_at, score, state_string, last_edited_id,
#last_activity_by_id, last_activity_at, active_revision_id, extra, extra_ref_id, extra_count, marked

def insertOrderedList(listData, item): 
    #inserts item into list in ascending order {list[0] is smallest item}
    #assumes list is already ordered
    #returns list with item inserted at appropriate location
    i = 0
    while (i < len(listData)):
        if listData[i] > item:
            listData.insert(i, item)
            return listData
        else:
            i += 1
    listData.append(item)
    return listData

def reducer():
    wordDict = dict()
    oldKey = None
#     reader = csv.reader(sys.stdin, delimiter='\t', quotechar='"')
#     writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    fin = open('forum_node_map.tsv','r')
    finW = open('forum_node_result.tsv','w')
    reader = csv.reader(fin, delimiter='\t', quotechar='"')
    writer = csv.writer(finW, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    for line in reader:
        thisKey, thisValue = line
        if thisKey in wordDict:
            wordDict[thisKey][0] += 1
            wordDict[thisKey][1].append(thisValue)
#             if thisKey not in wordDict[thisKey][1]:
#                 insertOrderedList(wordDict[thisKey][1],thisValue)
        else:
            wordDict[thisKey] = [1, [thisValue]]
    for keyWord in wordDict:
#         print keyWord + '\t' + str(wordDict[keyWord][0]) + '\t' + str(wordDict[keyWord][1])
        nodes = list(set(wordDict[keyWord][1]))
        nodes.sort()
        writer.writerow((keyWord,str(wordDict[keyWord][0]), nodes))
        
reducer()
