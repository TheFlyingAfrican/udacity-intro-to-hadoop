#! /usr/bin/python

#structure of forum post:
#id, title, tag_names, author_id, body, node_type, parent_id,  abs_parent_id, added_at, score, state_string, last_edited_id,
#last_activity_by_id, last_activity_at, active_revision_id, extra, extra_ref_id, extra_count, marked

import sys
import csv
import time

def mapper():
#     reader = csv.reader(open('forum_node.sample.tsv'), delimiter ='\t')
#     reader = csv.reader(open('forum_node.tsv'), delimiter ='\t')
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
#     writer = csv.writer(open('forum_node_output.tsv', 'w'), delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

    for line in reader:
        if line[0] == 'id':
            continue
        elif len(line) == 19: #reading forum data
            row = []
#             createDate = time.strptime(line[8][:19], '%Y-%m-%d %H:%M%S')
            postHour = line[8][11:13]
            row.append(line[3])
            row.append(postHour)
            writer.writerow(row)

mapper()