#!/usr/bin/python
import os
import sys
import csv

#structure of forum post:
#id, title, tag_names, author_id, body, node_type, parent_id,  abs_parent_id, added_at, score, state_string, last_edited_id,
#last_activity_by_id, last_activity_at, active_revision_id, extra, extra_ref_id, extra_count, marked

def splitString(dataString):
    punct = ' .!?:;"()<>[]#$=-/,\''
    wordList = []
    word = ''
    for i in range(0, len(dataString)):
        if dataString[i] in punct:
            if word != '':
                wordList.append(word.strip())
            word = ''
            continue
        else:
            word += dataString[i]
    if word != '':
        wordList.append(word.strip())
    return wordList

def mapper():
#     fin = open('forum_node.tsv','r')
#     finW = open('forum_node_map.tsv','w')
#     reader = csv.reader(fin, delimiter='\t', quotechar='"')
#     writer = csv.writer(finW, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    keywords = {}
    for line in reader:
        if len(line) != 19:
            continue
        else:
            body = splitString(line[4])
            for i in range(0, len(body)):
                if body[i].lower() in keywords:
                    keywords[body[i].lower()][0] += 1
                    keywords[body[i].lower()][1].append(line[0]) #insert regardless if it already exists
                else:
                    keywords[body[i].lower()] = [1,[line[0]]]
    for item in keywords:
#         #decompose dictionary into multiple (key,value) pair, for hadoop streaming we can't write (key,List<values>)
        for node in keywords[item][1]:
            writer.writerow((item, node))
#     finW.close()
    
mapper()