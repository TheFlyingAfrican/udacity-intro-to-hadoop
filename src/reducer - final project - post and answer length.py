#! /usr/bin/python

import sys
import csv

def reducer():
#     reader = csv.reader(open('forum_users.tsv'), delimiter ='\t')
#     reader = csv.reader(open('forum_node_output.tsv'), delimiter='\t')
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    forumDict = dict() #key = question_id; value = list contains question body length, # of answers, sum of answer length
    for line in reader:
        if line[1].lower() == 'question':
            if int(line[0]) in forumDict:
                forumDict[int(line[0])][0] =  int(line[2]) #update question body length
            else:
                forumDict[int(line[0])] = [int(line[2]), 0, 0] #create new entry 
        elif line[1].lower() == 'answer':
            if int(line[3]) in forumDict: #absolute parent id
                forumDict[int(line[3])][1] += 1
                forumDict[int(line[3])][2] += int(line[2])
            else:
                forumDict[int(line[3])] = [0, 1, int(line[2])] #create new entry 
    for item in forumDict: #calculate averages
        if forumDict[item][1] != 0:
            forumDict[item].append(float(forumDict[item][2]/forumDict[item][1]))
        else:
            forumDict[item].append(0.0)
        writer.writerow([item, forumDict[item][0], forumDict[item][3]])
#         print (forumDict[item])

reducer()