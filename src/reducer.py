#!/usr/bin/python
import os
import sys
import csv

#structure of forum post:
#id, title, tag_names, author_id, body, node_type, parent_id,  abs_parent_id, added_at, score, state_string, last_edited_id,
#last_activity_by_id, last_activity_at, active_revision_id, extra, extra_ref_id, extra_count, marked

def reducer():
    wordDict = dict()
#     oldKey = None
    reader = csv.reader(sys.stdin, delimiter='\t', quotechar='"')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
#     fin = open('forum_node_map.tsv','r')
#     finW = open('forum_node_result.tsv','w')
#     reader = csv.reader(fin, delimiter='\t', quotechar='"')
#     writer = csv.writer(finW, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
    for line in reader:
        thisKey, thisValue = line
        if thisKey in wordDict:
            wordDict[thisKey][0] += 1
            wordDict[thisKey][1].append(thisValue)
        else:
            wordDict[thisKey] = [1, [thisValue]]
    for keyWord in wordDict:
        nodes = list(set(wordDict[keyWord][1]))
        nodes.sort()
        writer.writerow((keyWord,str(wordDict[keyWord][0]), nodes))
        
reducer()
