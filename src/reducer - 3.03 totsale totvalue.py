#!/usr/bin/python

import sys

oldKey = None
totalSales = 0.0
totalNumSales = 0

# Loop around the data
# It will be in the format key\ tval
# Where key is the store name, val is the sale amount
#
# All the sales for a particular store will be presented,
# then the key will change and we'll be dealing with the next store
for line in sys.stdin:
    data_mapped = line.strip().split( "\t")
    if len(data_mapped) != 2:
        # Something has gone wrong. Skip this line.
        continue
    totalNumSales += 1
    thisKey, thisSale = data_mapped
    totalSales += float(thisSale)
    oldKey = thisKey

if oldKey != None :
    print oldKey, "\t", max_sale
