#!/usr/bin/python
# Your task is to write a mapper code that combines 2 datasets
# This is fairly involved task.
# You want to combine the datasets by joining them by the userid
# so, the mapper key should be "user_ptr_id" from "forum_users.tsv"
# and "author_id" from "forum_nodes.tsv" file. The value would be the full line
# from the respective files: either reputation and badges for the user,
# or full information about forum node.
# To be able to combine the records in the reducer you also need to know
# from which of the tables the informations comes from.
# So, the mapper should output A or B (or something similar) in front
# of the value. Output would be:
# 12345\tA"11"\t"0"\t"0"\t"0"
# 12345\tB"6336"\t"Unit 1: Same Value Q"\t"cs101 value same"  (etc...) 

# The reducer will get the values sorted, so the line starting with "A"
# will be information about the user, values starting with "B" will be forum nodes.
# Then you can store the user information, append this information to each forum node
# that this user had made, and print it out.

#structure of forum post:
#id, title, tag_names, author_id, body, node_type, parent_id,  abs_parent_id, added_at, score, state_string, last_edited_id,
#last_activity_by_id, last_activity_at, active_revision_id, extra, extra_ref_id, extra_count, marked

#structure of user
#user_ptr_id, reputation, gold, silver, bronze


import sys
import csv

def mapper():
#     reader = csv.reader(open('forum_node.sample.tsv'), delimiter ='\t')
#     reader = csv.reader(open('forum_users.tsv'), delimiter ='\t')
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

    for line in reader:
        if len(line) == 19: #reading forum data
            row = []
            row.append((str(line[3]) + 'sourceB'))
#             row.append(str('sourceB'))
            for i in range(0,3):
                row.append(line[i])
            for i in range(4,len(line)):
                row.append(line[i])
            writer.writerow(row) 
        elif len(line) == 5: #reading user data
            row = []
            user_ptr_id, reputation, gold, silver, bronze = line
            user_ptr_id = str(user_ptr_id) + 'sourceA'
            row.append(user_ptr_id)
#             row.append(str('sourceA'))
            for i in range(1, 5):
                row.append(line[i])
            writer.writerow(row)
        else: #something has gone wrong / malformed data
            continue

mapper()
        
# This function allows you to test the mapper with the provided test string
# def main():
#     import StringIO
#     sys.stdin = StringIO.StringIO(test_text)
#     mapper()
#     sys.stdin = sys.__stdin__
# 
# main()


#!/usr/bin/python
# Here you will be able to combine the values that come from 2 sources
# Value start starts with A will be the user data
# Values that start with B will be forum node data

import sys
import csv

def reducer():
    reader = csv.reader(sys.stdin, delimiter='\t')
    writer = csv.writer(sys.stdout, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
#     reader = csv.reader(open('combiningdata.txt','r'), delimiter='\t')

    thisUserRow = None
    previousUserRow = None
    previousUser = None
    previousLine = None
    thisLine = None
    for line in reader:
        if (len(line) == 5) and (line[0][len(line[0])-7:] == 'sourceA'): #is forum user data
            line[0] = line[0][0:len(line[0]-7)]
            thisUserRow = line
            if thisUserRow and thisUserRow != previousUserRow:
                previousUserRow = thisUserRow
        elif (len(line) == 19) and (line[0][len(line[0])-7:]  == 'sourceB'): #is forum node data
            line[0] = line[0][0:len(line[0]-7)]
            if thisUserRow[0] == line[0]:
                for item in thisUserRow:
                    line.append(item)
                writer.writeout(line)
        else: #unrecognized case
            continue

reducer()